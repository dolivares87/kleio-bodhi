Kleio

The archiver takes your old records and saves them for you. Bc it is what you wish.

Under Construction

Customizable. 
You configure multiple-file paths OR a path glob.
Defaults saves to S3 after zipping it. Uses namespace/date by default. Variables allowed are also:
storeid, filename, filetype.
Returns the path it saved the records too.
Uses bodhi's client!
Uses streams!

kleio: opts(
  client,
  filesToArchive: [] or 'filename',
  dateRange: [to, from] or default older than 3 weeks,
  onFail(fn),
  onSuccess(fn),
  stream?,
  done(fn);
)

kleio
  .archive(filename)
  .done(); --> returns [] || '' of filepath stored too
