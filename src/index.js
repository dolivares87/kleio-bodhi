import 'source-map-support/register';
import moment from 'moment';
import bodhiClient from 'bodhi-client-superagent-promisized';
import optParser from './lib/optionParser';
import createJobOpt from './lib/createJobOpt';
import isArchiveOptString from './lib/isArchiveOptString';
import runArchiveJobs from './lib/runArchiveJobs';
import archive from './lib/archive';

function cleanResult(result, finalizeFunc) {
  const cleanedResult = result.filter(uploadPath => uploadPath);

  return finalizeFunc(null, cleanedResult);
}

export default function kleio(opts = {}) {
  const { client: normalClient } = opts;

  const options = Object.assign({}, opts, {
    client: bodhiClient.promisizeClient(normalClient)
  });

  return Object.assign(Object.create({
    archive(archiveOpts) {
      const archiveDate = this.archiveDate || moment();
      // TODO: Make sure that the date use is passed in as a momentjs date or turn it into one
      const parsedOpts = isArchiveOptString(archiveOpts) ? optParser(archiveOpts) : archiveOpts;

      this.pendingJobs = parsedOpts.map(archiveOpt => createJobOpt(archiveDate, archiveOpt));

      return this;
    },
    finalize(fn) {
      const pendingJobs = this.pendingJobs;
      const client = this.client;
      const archiveJobs = runArchiveJobs(pendingJobs, archive, client);

      archiveJobs
        .then(result => cleanResult(result, fn))
        .catch(err => fn(err));
    }
  }), options);
}
