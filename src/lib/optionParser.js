export default function optionParser(configString = '') {
  const options = configString
    .replace(' ', '')
    .split(',');

  const parsedOptions = options.map(opt => {
    const [type, dateProperty, olderThan] = opt.split(':');

    if (olderThan) {
      return { type, dateProperty, olderThan: parseInt(olderThan, 10) };
    }

    return { type, dateProperty };
  });

  return parsedOptions;
}
