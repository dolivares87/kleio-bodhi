export default function isArchiveOptString(optString = '') {
  return typeof optString === 'string';
}
