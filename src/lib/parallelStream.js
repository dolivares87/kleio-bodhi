
/* eslint-disable */
// import { Transform } from 'stream';

// class ParallelStream extends Transform {
//   constructor() {
//     super();
//     this.running = 0;
//     this.terminateCallback = null;
//   }
//   _transform(chunk, enc, done) {
//     this.running++;
//     this.userTransform(chunk, enc, this._onComplete.bind(this));
//     done();
//   }
//   _flush(done) {
//     if (this.running > 0) {
//       this.terminateCallback = done;
//     } else {
//       done();
//     }
//   }
//   _onComplete(err) {
//     this.running--;
//
//     if (err) {
//       return this.emit('error', err);
//     }
//
//     if (this.running === 0) {
//       this.terminateCallback && this.terminateCallback();
//     }
//   }
// }

// export default ParallelStream;
// export default function parallelStream(transform) {
//   const pStream = {
//     running: 0,
//     terminateCallback: null
//   };
//
//   Transform.call(pStream, { objectMode: true });
//
//   Object.assign(pStream, Transform.prototype);
// }
