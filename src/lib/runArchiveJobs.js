async function runJob(fetchDataUrl, uploadPath, archiveFn, client) {
  const allRecords = await client.getAll(fetchDataUrl);

  if (!allRecords.length) {
    return '';
  }

  const archivedContent = await archiveFn(JSON.stringify(allRecords));

  // Upload the content to the specified path
  await client.file.uploadContent(uploadPath, archivedContent);

  return uploadPath;
}

export default function runArchiveJobs(jobs, archiveFn, client) {
  const jobsToRun = jobs.map(jobOpts => {
    const { fetchDataUrl, uploadPath } = jobOpts;

    return runJob(fetchDataUrl, uploadPath, archiveFn, client);
  });

  return Promise.all(jobsToRun);
}
