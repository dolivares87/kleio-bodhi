import moment from 'moment';

export default function createJobOpt(archiveDate, archiveOpt = {}) {
  const { type, dateProperty, olderThan = 3 } = archiveOpt;
  const olderThanDate = moment(archiveDate).subtract(olderThan, 'weeks');
  const olderThanIso = olderThanDate.toISOString();
  const olderThanFormatted = olderThanDate.format('YYYYMMDD');

  const fetchDataUrl = `resources/${type}?where={${dateProperty}:{$lte:{$date:"${olderThanIso}"}}}`;
  const uploadPath = `${type}/${olderThanFormatted}-older`;

  return {
    fetchDataUrl,
    uploadPath
  };
}
