import zlib from 'zlib';
import promisize from 'promisize';

const defaultEncodeType = 'base64';

export default async function archive(records = []) {
  const stringyRecs = JSON.stringify(records);
  const gzip = promisize({
    target: zlib.gzip
  });

  const recordsBuffer = await gzip(stringyRecs);

  return recordsBuffer.toString(defaultEncodeType);
}
