export default function encodeBuffer({ bufferToEncode, encodeType = 'base64' }) {
  return bufferToEncode.toString(encodeType);
}
