import debug from 'debug';

const APP_NAME = 'kleio';

export default function bugger(debugSource = '') {
  let debugName = `${APP_NAME}`;

  if (debugSource) {
    debugName = `${debugName}:${debugSource}`;
  }

  return debug(debugName);
}
