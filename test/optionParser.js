import test from 'ava';
import optionParser from '../src/lib/optionParser';

test('optionParser(optionString) with no olderThan section', t => {
  const optionString = 'testFiles:sortDate';
  const expected = [{
    type: 'testFiles',
    dateProperty: 'sortDate'
  }];

  const parsedOpts = optionParser(optionString);

  t.deepEqual(parsedOpts, expected, 'Should return array of optionObject from optionString');
});

test('optionParser(optionString) with custom olderThan section', t => {
  const optionString = 'otherTestFiles:useDate:5';
  const expected = [{
    type: 'otherTestFiles',
    dateProperty: 'useDate',
    olderThan: 5
  }];

  const parsedOpts = optionParser(optionString);

  t.deepEqual(parsedOpts, expected, 'Should return array of optionObjects with custom olderThan');
});
