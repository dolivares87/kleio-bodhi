import test from 'ava';
import createJobOpt from '../src/lib/createJobOpt';
import basicData from './fixtures/basicTestData';

test('createJobOpt(archiveOpt) with default olderThan prop', t => {
  const {
    date,
    fetchThreeWeeks,
    threeWeeksBackUploadPath
  } = basicData();

  const archiveOpt = {
    type: 'testFiles',
    dateProperty: 'sortDate'
  };

  const expected = {
    fetchDataUrl: fetchThreeWeeks,
    uploadPath: threeWeeksBackUploadPath
  };

  const jobOpt = createJobOpt(date, archiveOpt);

  t.deepEqual(jobOpt, expected, 'Should return a job opt with a default olderThan of 3');
});

test('createJobOpt(archiveOpt) with custom olderThan prop', t => {
  const {
    date,
    fetchFiveWeeks,
    fiveWeeksBackUploadPath
  } = basicData();

  const archiveOpt = {
    type: 'otherTestFiles',
    dateProperty: 'useDate',
    olderThan: 5
  };

  const expected = {
    fetchDataUrl: fetchFiveWeeks,
    uploadPath: fiveWeeksBackUploadPath
  };

  const jobOpt = createJobOpt(date, archiveOpt);

  t.deepEqual(jobOpt, expected, 'Should return a job opt with a default olderThan of 3');
});
