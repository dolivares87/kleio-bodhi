import createResourceClient from './integration/resource';
import creacteNamespaceClient from './integration/namespace';


export default function createHelperClients(namespace, testType) {
  const namespaceCred = `admin__${namespace}`;
  const typeBody = {
    namespace,
    name: testType,
    embedded: false,
    properties: {
      contentUrl: {
        type: 'String',
        required: false
      },
      content: {
        type: 'String',
        required: false
      }
    },
    indexes: []
  };
  const typeContent = {
    contentUrl: 'this/is/a/fake/url',
    content: 'this is fake as heck content just testing this stuff yay'
  };

  const resourceCreds = {
    userName: namespaceCred,
    passWord: namespaceCred
  };

  const resourceClientOpts = {
    creds: resourceCreds,
    namespace,
    testType,
    typeBody,
    typeContent
  };

  const spaceMakercreds = {
    userName: 'System',
    passWord: 'Sy5t3mP@s5w0rd'
  };

  const spaceMakerOpts = {
    name: namespace,
    creds: spaceMakercreds,
    nameSpaceOpts: {
      namespace,
      organization: 'DELETE THIS',
      customerId: '876'
    }
  };

  return {
    resourceClient: createResourceClient(resourceClientOpts),
    spaceMakerClient: creacteNamespaceClient(spaceMakerOpts)
  };
}
