import superagent from 'superagent';
import promisize from 'promisize';
import superAgentPromisePlugin from 'superagent-promise-plugin';

const request = superAgentPromisePlugin.patch(superagent);

export default function resource(opts = {}) {
  const { creds: { userName, passWord }, namespace, testType, typeBody, typeContent } = opts;
  const url = `https://api.bodhi-qa.io/${namespace}`;

  return {
    createTestType(typeDef) {
      return request
        .post(`${url}/types`)
        .auth(userName, passWord)
        .send(typeBody || typeDef);
    },
    uploadContentToTestType(content, typeName) {
      return request
        .post(`${url}/resources/${testType || typeName}`)
        .auth(userName, passWord)
        .send(typeContent || content);
    },
    deleteUploadedTestFile(pathToDelete = '') {
      return request
        .delete(`${url}/controllers/vertx/upload/${pathToDelete}`)
        .auth(userName, passWord);
    },
    downloadUpoloadedTestFile(bodhiClient, pathToDownload = '') {
      const downloadFile = promisize({
        target: bodhiClient.file.download
      });

      return downloadFile(pathToDownload);
    }
  };
}
