import superagent from 'superagent';
import superAgentPromisePlugin from 'superagent-promise-plugin';

const request = superAgentPromisePlugin.patch(superagent);

function createUrl(action) {
  return `https://api.bodhi-qa.io/system/controllers/vertx/resources/namespaceauthority/${action}`;
}

export default function namespace(opts = {}) {
  const { creds: { userName, passWord }, name, nameSpaceOpts } = opts;

  return {
    createNamespace() {
      return request
        .post(createUrl('create'))
        .timeout(10000)
        .auth(userName, passWord)
        .send(nameSpaceOpts);
    },
    deleteNamespace() {
      return request
        .post(createUrl('delete?hard=true'))
        .auth(userName, passWord)
        .send({ namespace: name });
    }
  };
}
