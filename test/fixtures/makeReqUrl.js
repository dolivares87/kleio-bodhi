export default function makeReqUrl(fileName, property, dateToUse) {
  return `resources/${fileName}?where={${property}:{$lte:{$date:"${dateToUse}"}}}`;
}
