import testFiles from './files/testFiles.json';
import otherTestFiles from './files/otherTestFiles.json';

// Batman_bdi306
export default function createClient(basicData) {
  const {
    fetchThreeWeeks,
    fetchFiveWeeks,
    threeWeeksBackUploadPath,
    fiveWeeksBackUploadPath
  } = basicData;

  return {
    getAll(reqUrl, callback) {
      if (reqUrl === fetchThreeWeeks) {
        setTimeout(() => {
          callback(null, testFiles);
        }, 2);
      } else if (reqUrl === fetchFiveWeeks) {
        setTimeout(() => {
          callback(null, otherTestFiles);
        }, 2);
      } else {
        setTimeout(() => {
          callback('No Files');
        }, 2);
      }
    },
    file: {
      uploadContent(reqUrl, content, callback) {
        if (reqUrl === threeWeeksBackUploadPath || reqUrl === fiveWeeksBackUploadPath) {
          setTimeout(() => {
            callback(null);
          }, 2);
        } else {
          setTimeout(() => {
            callback('Failed upload');
          }, 2);
        }
      }
    }
  };
}
