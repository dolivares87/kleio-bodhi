import moment from 'moment';
import makeReqUrl from './makeReqUrl';

export default function basicTestData() {
  const date = moment();
  const threeWeeksBack = moment(date).subtract(3, 'week');
  const fiveWeeksBack = moment(date).subtract(5, 'weeks');
  const threeWeeksBackUploadPath = `testFiles/${threeWeeksBack.format('YYYYMMDD')}-older`;
  const fiveWeeksBackUploadPath = `otherTestFiles/${fiveWeeksBack.format('YYYYMMDD')}-older`;
  const fetchThreeWeeks = makeReqUrl('testFiles', 'sortDate', threeWeeksBack.toISOString());
  const fetchFiveWeeks = makeReqUrl('otherTestFiles', 'useDate', fiveWeeksBack.toISOString());

  return {
    date,
    threeWeeksBack,
    fiveWeeksBack,
    fetchThreeWeeks,
    fetchFiveWeeks,
    threeWeeksBackUploadPath,
    fiveWeeksBackUploadPath
  };
}
