/* eslint-disable */
import MICROS_trans_dtl from './files/MICROS_trans_dtl.json';
import MICROS_sale_dtl from './files/MICROS_sale_dtl.json';
import MICROS_rvc_def from './files/MICROS_rvc_def.json';
import MICROS_trans_dtl_filtered from './files/MICROS_trans_dtl-filtered.json';
/* eslint-enable */

const FILE_CHOICES = {
  MICROS_trans_dtl,
  MICROS_sale_dtl,
  MICROS_rvc_def
};

function handleFiltered(url) {
  const filterString = '?where={start_date_tm:{$gte:{$date:"2016-05-01T00:00:00.000Z"}}}';
  return url.indexOf(filterString) !== -1;
}

export default function client() {
  return {
    getAll(reqUrl, callback) {
      if (handleFiltered(reqUrl)) {
        /* eslint-disable */
        return callback(null, MICROS_trans_dtl_filtered);
        /* eslint-enable */
      }
      const fileName = reqUrl.split('/')[1];

      if (fileName === 'None') {
        return callback(null, []);
      } else if (FILE_CHOICES[fileName]) {
        return callback(null, FILE_CHOICES[fileName]);
      }

      return callback('NO FILE');
    },
    file: {
      uploadContent(uploadPath, content, callback) {
        callback(null, uploadPath);
      }
    }
  };
}
