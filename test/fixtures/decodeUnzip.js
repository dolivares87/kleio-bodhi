import zlib from 'zlib';
import promisize from 'promisize';

export default async function decodeUnzip(contentBuffer) {
  const buffer = Buffer.from(contentBuffer.toString(), 'base64');
  const gunzip = promisize({
    target: zlib.gunzip
  });

  const content = await gunzip(buffer);

  return content.toString();
}
