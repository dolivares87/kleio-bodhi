import test from 'ava';
import kleio from '../src';
import basicTestData from './fixtures/basicTestData';
import createClient from './fixtures/createClient';

test.cb('kleio runs and stores compressed data', t => {
  const basicData = basicTestData();
  const {
    date,
    threeWeeksBackUploadPath,
    fiveWeeksBackUploadPath
  } = basicData;

  const client = createClient(basicData);
  const optionString = 'testFiles:sortDate, otherTestFiles:useDate:5';
  const expected = [threeWeeksBackUploadPath, fiveWeeksBackUploadPath];

  const kleioOpts = {
    client,
    archiveDate: date
  };

  kleio(kleioOpts)
    .archive(optionString)
    .finalize((err, uploadedPaths) => {
      t.deepEqual(uploadedPaths, expected, 'Should return uploadedPaths');
      t.end();
    });
});

test.cb('kleio runs and stores compressed data from option array', t => {
  const basicData = basicTestData();
  const {
    date,
    threeWeeksBackUploadPath,
    fiveWeeksBackUploadPath
  } = basicData;

  const client = createClient(basicData);
  const optionArray = [{
    type: 'testFiles',
    dateProperty: 'sortDate'
  }, {
    type: 'otherTestFiles',
    dateProperty: 'useDate',
    olderThan: 5
  }];
  const expected = [threeWeeksBackUploadPath, fiveWeeksBackUploadPath];

  const kleioOpts = {
    client,
    archiveDate: date
  };

  kleio(kleioOpts)
    .archive(optionArray)
    .finalize((err, uploadedPaths) => {
      t.deepEqual(uploadedPaths, expected, 'Should return uploadedPaths');
      t.end();
    });
});
