import test from 'ava';
import moment from 'moment';
import createClient from './fixtures/integration/createClient';
import integrationClient from './fixtures/integrationClient';
import decodeUnzip from './fixtures/decodeUnzip';
import promisize from 'promisize';
import kleio from '../src';

const date = moment(new Date()).format('YYYYMMDD');
const namespace = 'kleio__integrationtest';
const testType = 'kleiotesttype';
const testTypeUploadUrl = `${testType}/${date}`;
const { spaceMakerClient, resourceClient } = integrationClient(namespace, testType);

function createNamespaceWorkaround() {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await spaceMakerClient.createNamespace();

      resolve(result);
    } catch (e) {
      if (e.timeout === 10000) {
        resolve();
      } else {
        reject(e);
      }
    }
  });
}

test.before(async () => {
  try {
    await createNamespaceWorkaround();
  } catch (e) {
    console.log('Error in creating pre-test reqs', e);
    return;
  }

  try {
    await resourceClient.createTestType();
    await resourceClient.uploadContentToTestType();
  } catch (e) {
    console.log('Error in adding types to resource', e);
  }
});

test('Archive live records', async t => {
  const expectedPath = `kleiotesttype/${date}`;
  const expected = [expectedPath];
  const bodhiClient = createClient({
    uri: 'https://api.bodhi-qa.io',
    namespace,
    creds: {
      userName: `admin__${namespace}`,
      passWord: `admin__${namespace}`
    }
  });
  const archiver = kleio(
    bodhiClient
  );

  const storedToUrl = await archiver.archive({
    types: testType
  });

  t.deepEqual(storedToUrl, expected, 'Should return the path the file was stored to.');

  const getAll = promisize({
    target: bodhiClient.getAll
  });

  const uploadedContent = await resourceClient.downloadUpoloadedTestFile(bodhiClient, expectedPath);
  const decoded = await decodeUnzip(uploadedContent);
  const archivedRecords = await getAll('resources/kleiotesttype');

  t.deepEqual(JSON.parse(decoded), archivedRecords,
    'Should match all the records that were archived and retrieved');
});

test.after.always(async () => {
  try {
    await resourceClient.deleteUploadedTestFile(testTypeUploadUrl);
    await spaceMakerClient.deleteNamespace();
  } catch (e) {
    console.log(`Failed to cleanup namespace ${namespace}`, e);
  }
});
