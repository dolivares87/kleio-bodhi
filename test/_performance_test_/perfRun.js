import namespaceClient from '../fixtures/integration/namespace';
import resourceClient from '../fixtures/integration/resource';
// import microsTransDtlType from './types/MICROS_trans_dtl.json';
// import microsRVCDef from './types/MICROS_rvc_def.json';
// import microsSaleDtl from './types/MICROS_sale_dtl.json';
// import microsTransDtlContent from '../fixtures/files/MICROS_trans_dtl.json'
// import microsRVCContent from '../fixtures/files/MICROS_rvc_def.json'
// import microsSalesDtlContent from '../fixtures/files/MICROS_sale_dtl.json'
import createClient from '../fixtures/integration/createClient';
import promisize from 'promisize';
import appMet from 'appmetrics';
import kleio from '../../dist/';
import bug from 'debug';
const debugCPU = bug('perf:CPU');
const debugMEM = bug('perf:MEM');

const namespace = 'kleio__perfTest';
const spaceMakercreds = {
  userName: 'System',
  passWord: 'Sy5t3mP@s5w0rd'
};

const spaceMakerOpts = {
  name: namespace,
  creds: spaceMakercreds,
  nameSpaceOpts: {
    namespace,
    organization: 'DELETE THIS',
    customerId: '8666'
  }
};

const namespaceCred = `admin__${namespace}`;
const resourceCreds = {
  userName: namespaceCred,
  passWord: namespaceCred
};
const resourceClientOpts = {
  creds: resourceCreds,
  namespace
};

const namespaceCreator = namespaceClient(spaceMakerOpts);
const resourceHandler = resourceClient(resourceClientOpts);

async function createTestNamespace() {
  return await namespaceCreator.createNamespace();
}

const bodhiClient = createClient({
  uri: 'https://api.bodhi-qa.io',
  namespace,
  creds: {
    userName: `admin__${namespace}`,
    passWord: `admin__${namespace}`
  }
});

async function createAndLoadTypes() {
  const post = promisize({
    target: bodhiClient.post
  });

  try {
    // await resourceHandler.createTestType(microsTransDtlType);
    console.log('count', microsRVCContent.length);
    console.log('count', microsSalesDtlContent.length);
    // return await post('resources/MICROS_trans_dtl', microsTransDtlContent);
    // await resourceHandler.createTestType(microsRVCDef);
    // await post('MICROS_rvc_def', microsRVCContent);
    // await resourceHandler.createTestType(microsSaleDtl);
    // await post('resources/MICROS_sale_dtl', microsSalesDtlContent);
  } catch (e) {
    throw e;
  }
}

const archiver = kleio(bodhiClient);

const monitoring = appMet.monitor();
monitoring.on('initialized', function (env) {
    env = monitoring.getEnvironment();
    for (var entry in env) {
        console.log(entry + ':' + env[entry]);
    };
});

monitoring.on('cpu', function (cpu) {
  debugCPU('[ Process ' + new Date(cpu.time) + '] CPU: ' + (cpu.process * 100 * 10) + '%');
});

monitoring.on('memory', function (mem) {
  debugMEM('[ Process Memory' + new Date(mem.time) + '] MEM: ' + ((mem.physical/mem.physical_total) * 100) + '%');
});

async function runPerfTest() {
  console.log('PID', process.pid);
  return await archiver.archive({ types: ['MICROS_trans_dtl', 'MICROS_rvc_def', 'MICROS_sale_dtl'] });
}

// createAndLoadTypes()
//   .then(() => console.log('done'))
//   .catch(err => console.log('err', err));
runPerfTest()
.then(done => console.log('donel', done))
.catch(err => console.log('err', err));
