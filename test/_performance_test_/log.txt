2016-06-16 - Rand MICROS_trans_dtl file process. Peak CPU about 56% for 1 second while transforming data received. 
Before that, peaked at about 9% cpu. Memory always low.

Second run with all three test files. Peaked at about 65% for a couple of seconds. Before that, peaked at about 9% cpu. Memory seems to have been kept low overall.
