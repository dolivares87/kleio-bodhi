/* eslint-disable */
var microsDtl = require('../fixtures/files/MICROS_trans_dtl.json');
var microsRVC = require('../fixtures/files/MICROS_rvc_def.json');
var microsSaleDtl = require('../fixtures/files/MICROS_sale_dtl.json');
var kleio = require('../../dist').default;

var client = {
  getAll(reqUrl, callback) {
    console.log(reqUrl);
    callback(null, microsDtl);
  },
  file: {
    upload(uploadPath, content, callback) {
      callback(null, uploadPath);
    }
  }
}

var archiver = kleio(client);

const callback =  (err, data) => {
  console.log('data', data);
}

archiver.archive({
  type: ['MICROS_trans_dtl']
}, callback);
